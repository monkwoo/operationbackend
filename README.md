# Database and APIs #

DB Models

### Incident Report ###

* incidentBody
* incidentImage
* incidentHeader

### Channels ###

* id
* logo
* themeColor
* displayName

### GoodsInfo ###

* goodName
* goodPrice
* goodOldPrice
* goodImage

### PlatformPartner ###

* id
* position
* isVisible
* partnerUrl
* partnerName
* partnerImage

### PlatformConfig ###

* supportNumber

### Location ###

### LineItem ###

### BulkOrder ###

### User ###

### Donation ###

