'use strict';

const express = require('express');
const path = require('path');

/** Middleware **/
const bodyParser= require('body-parser');

/** Define app and plugin with middlewares **/
const app = express();
app.use(
    /** support json encoded bodies **/
    bodyParser.json()
);
app.use(
    /** support encoded bodies **/
    bodyParser.urlencoded(
        { extended: true }
    )
);
app.use(
    /** support static media content **/
    express.static(
        path.join(__dirname, 'webpublic')
    )
);


let __database;
const mongodb = require('mongodb');

/** Env Configuration **/
let __app_config;
if(
    process.env.LOCATION != 'prod'
){
    console.log(
        '-----------------',
        'Working in local',
        '-----------------'
    );
    __app_config = require('./config');
}else{
    console.log(
        '------------------------',
        'Production Config loaded',
        '------------------------'
    );
    __app_config = require('./config_prod');
}

mongodb.MongoClient.connect(
    __app_config.db_url,
    (err, database) => {
        __database = database;
        app.listen(
            __app_config.app_port,
            e => console.log(`listening on ${__app_config.app_port}`)
        );
    }
);

/** UI URLS **/
app.get(
    '/public-portal/demo',
    (request, response) => {
        console.log('GET /public-portal/demo', request.user);
        response.sendFile(
            path.join(__dirname, 'webpublic', 'index.html')
        );
    }
);

app.get(
    '/public-portal/demo/edit',
    (request, response) => {
        console.log('GET /public-portal/demo/edit', request.user);
        response.sendFile(
            path.join(__dirname, 'webpublic', 'editIndex.html')
        );
    }
);


/** APIs **/

/**
 * URL : '/'
 **/
app.get(
    '/',
    (request, response) => {
        console.log('GET /', request.user);
        //response.send('Operation Resilience.');
        response.sendFile(
            path.join(__dirname, 'webpublic', 'index.html')
        );
    }
);

/**
 * URL : '/channels'
 **/
app.get(
    '/channels/:id?',
    (request, response) => {
        console.log(
            `GET /channels/${request.params.id}`,
            request.user
        );
        let __channel_query = {};
        if(request.params.id){
            __channel_query['_id'] = mongodb.ObjectID(request.params.id);
        }
        __database.collection('channels').find(
                __channel_query,
                {}
            ).toArray().then(
            channel => {
                response.send(channel);
            }
        );
    }
);

app.post(
    '/channels',
    (request, response) => {
        console.log('POST /channels', request.user);
        /** This to do here
         *  Check if data is not null
         *  Check if channel already exists with provided data
         *  if it does return id else save and return id
         *  in case of error return error message
         **/
        __database.collection('channels').save(
            {
                "themeColor": request.body.themeColor,
                "displayName": request.body.displayName,
                "logo": request.body.logo
            },
            (err, result) => {
                if(err){
                    console.log(
                        'error to save to database',
                        err
                    );
                    response.send(
                        {
                            'status': 'failed',
                            'error': err.message
                        }
                    );
                }else{
                    console.log('saved to database', result);
                    response.send(
                        {
                            'status': 'saved',
                            'id': result.ops[0]._id,
                        }
                    );
                }
            }
        );
    }
);

/**
 * URL : '/incidentreport'
 **/
app.get(
    '/incidentreport/:id?',
    (request, response) => {
        console.log(
            `GET /incidentreport/${request.params.id}`,
            request.user
        );
        let __incidentreport_query = {};
        if(request.params.id){
            __incidentreport_query['_id'] = mongodb.ObjectID(request.params.id);
        }
        __database.collection('incidentreport').find(
                __incidentreport_query,
                {}
            ).toArray().then(
            incidentreport => {
                response.send(incidentreport);
            }
        );
    }
);

app.post(
    '/incidentreport',
    (request, response) => {
        console.log('POST /incidentreport', request.user);
        /** This to do here
         *  Check if data is not null
         *  Check if incidentreport already exists with provided data
         *  if it does return id else save and return id
         *  in case of error return error message
         **/
        __database.collection('incidentreport').save(
            {
                "incidentBody": request.body.incidentBody,
                "incidentImage": request.body.incidentImage,
                "incidentHeader": request.body.incidentHeader
            },
            (err, result) => {
                if(err){
                    console.log(
                        'error to save to database',
                        err
                    );
                    response.send(
                        {
                            'status': 'failed',
                            'error': err.message
                        }
                    );
                }else{
                    console.log('saved to database', result);
                    response.send(
                        {
                            'status': 'saved',
                            'id': result.ops[0]._id,
                        }
                    );
                }
            }
        );
    }
);


/**
 * URL : '/goodsinfo'
 **/
app.get(
    '/goodsinfo/:id?',
    (request, response) => {
        console.log(
            `GET /goodsinfo/${request.params.id}`,
            request.user
        );
        let __goodsinfo_query = {};
        if(request.params.id){
            __goodsinfo_query['_id'] = mongodb.ObjectID(request.params.id);
        }
        __database.collection('goodsinfo').find(
                __goodsinfo_query,
                {}
            ).toArray().then(
            goodsinfo => {
                response.send(goodsinfo);
            }
        );
    }
);

app.post(
    '/goodsinfo',
    (request, response) => {
        console.log('POST /goodsinfo', request.user);
        /** This to do here
         *  Check if data is not null
         *  Check if goodsinfo already exists with provided data
         *  if it does return id else save and return id
         *  in case of error return error message
         **/
        __database.collection('goodsinfo').save(
            {
                "goodName": request.body.goodName,
                "goodPrice": request.body.goodPrice,
                "goodOldPrice": request.body.goodOldPrice,
                "goodImage": request.body.goodImage
            },
            (err, result) => {
                if(err){
                    console.log(
                        'error to save to database',
                        err
                    );
                    response.send(
                        {
                            'status': 'failed',
                            'error': err.message
                        }
                    );
                }else{
                    console.log('saved to database', result);
                    response.send(
                        {
                            'status': 'saved',
                            'id': result.ops[0]._id,
                        }
                    );
                }
            }
        );
    }
);


/**
 * URL : '/platformpartner'
 **/
app.get(
    '/platformpartner/:id?',
    (request, response) => {
        console.log(
            `GET /platformpartner/${request.params.id}`,
            request.user
        );
        let __platformpartner_query = {};
        if(request.params.id){
            __platformpartner_query['_id'] = mongodb.ObjectID(request.params.id);
        }
        __database.collection('platformpartner').find(
                __platformpartner_query,
                {}
            ).toArray().then(
            platformpartner => {
                response.send(platformpartner);
            }
        );
    }
);

app.post(
    '/platformpartner',
    (request, response) => {
        console.log('POST /platformpartner', request.user);
        /** This to do here
         *  Check if data is not null
         *  Check if platformpartner already exists with provided data
         *  if it does return id else save and return id
         *  in case of error return error message
         **/
        __database.collection('platformpartner').save(
            {
                "position": request.body.position,
                "isVisible": request.body.isVisible,
                "partnerUrl": request.body.partnerUrl,
                "partnerImage": request.body.partnerImage,
                "partnerName": request.body.partnerName
            },
            (err, result) => {
                if(err){
                    console.log(
                        'error to save to database',
                        err
                    );
                    response.send(
                        {
                            'status': 'failed',
                            'error': err.message
                        }
                    );
                }else{
                    console.log('saved to database', result);
                    response.send(
                        {
                            'status': 'saved',
                            'id': result.ops[0]._id,
                        }
                    );
                }
            }
        );
    }
);


/**
 * URL : '/platformpartner'
 **/
app.get(
    '/platformpartner/:id?',
    (request, response) => {
        console.log(
            `GET /platformpartner/${request.params.id}`,
            request.user
        );
        let __platformpartner_query = {};
        if(request.params.id){
            __platformpartner_query['_id'] = mongodb.ObjectID(request.params.id);
        }
        __database.collection('platformpartner').find(
                __platformpartner_query,
                {}
            ).toArray().then(
            platformpartner => {
                response.send(platformpartner);
            }
        );
    }
);

app.post(
    '/platformpartner',
    (request, response) => {
        console.log('POST /platformpartner', request.user);
        /** This to do here
         *  Check if data is not null
         *  Check if platformpartner already exists with provided data
         *  if it does return id else save and return id
         *  in case of error return error message
         **/
        __database.collection('platformpartner').save(
            {
                "position": request.body.position,
                "isVisible": request.body.isVisible,
                "partnerUrl": request.body.partnerUrl,
                "partnerImage": request.body.partnerImage,
                "partnerName": request.body.partnerName
            },
            (err, result) => {
                if(err){
                    console.log(
                        'error to save to database',
                        err
                    );
                    response.send(
                        {
                            'status': 'failed',
                            'error': err.message
                        }
                    );
                }else{
                    console.log('saved to database', result);
                    response.send(
                        {
                            'status': 'saved',
                            'id': result.ops[0]._id,
                        }
                    );
                }
            }
        );
    }
);

