'use strict';

const express = require('express');

/** Middleware **/
const bodyParser= require('body-parser');

/** Define app and plugin with middlewares **/
const app = express();
app.use(
    bodyParser.json()
); // support json encoded bodies
app.use(
    bodyParser.urlencoded(
        { extended: true }
    )
); // support encoded bodies

let __database;

const MongoClient = require('mongodb').MongoClient
MongoClient.connect(
    'mongodb://localhost:27017/operationbackend',
    (err, database) => {
        __database = database;
        app.listen(
            3000,
            e => console.log('listening on 3000')
        );
    }
)

app.get(
    '/',
    (request, response) => {
        console.log('GETing /')
        //response.send('Hello world');
        response.sendFile(
            `${__dirname}/index.html`
        );
    }
);

app.get(
    '/hello',
    (request, response) => {
        console.log('GETing /hello');
        let p = [];
        __database.collection('hello').find().toArray(
            (err, result) => {
                console.log(result);
                p.push(result);
            }
        );
        setTimeout(
            _ => response.send(JSON.stringify(p)),
            3000
        );
    }
);

app.post(
    '/hello',
    (request, response) => {
        console.log('Posting /hello', request.body);
        __database.collection('hello').save(
            {
                "query": request.query,
                "body": request.body,
                "params": request.params
            },
            (err, result) => {
                if (err) return console.log(err)

                console.log('saved to database');
                response.redirect('/hello');
            }
        );
    }
);
